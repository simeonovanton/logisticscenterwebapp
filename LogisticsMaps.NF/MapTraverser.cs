﻿using System;
using System.Collections.Generic;
using System.Text;
using Wintellect.PowerCollections;

namespace ConsoleLogisticsMap
{
    class MapTraverser
    {
        //This method calculates the shortest distance to all other cities by Dijkstra algorithm
        public static void RunDijkstraFromStartCity(Map map, string cityName)
        {
            City startCity = map.Cities[cityName];
            startCity.BestDistance = 0; // Zeroing this property before traversing algorithm
            OrderedBag<City> priorityQueue = new OrderedBag<City>();

            priorityQueue.Add(startCity);

            while (priorityQueue.Count > 0)
            {
                City city = priorityQueue.RemoveFirst();
                city.Visited = true;
                foreach (var road in city.ChildCities)
                {
                    City childCity = road.Key;
                    int childRoad = road.Value;
                    if (childCity.Visited)
                    {
                        continue;
                    }
                    int newDistance = city.BestDistance + childRoad;
                    if (newDistance < childCity.BestDistance)
                    {
                        priorityQueue.Remove(childCity); // removes it - IF exists /see OrderedBag/

                        childCity.BestDistance = newDistance;
                        childCity.LastCity = city;

                        priorityQueue.Add(childCity);

                        if (newDistance > startCity.MaxDistance)
                        {
                            startCity.MaxDistance = newDistance;
                            startCity.MaxDistanceCity = childCity;
                        }
                    }
                }
            }
        }
    }
}
