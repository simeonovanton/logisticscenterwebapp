﻿using DBLogisticsMap.CodeFirst;
using System;
using System.IO;
using System.Linq;

namespace ConsoleLogisticsMap
{
    class MainApp
    {
        
        static void Main()
        {
            //var context = new LogisticsMapContext();
            //var map = context.Maps.ToList();
            //Console.WriteLine();


            Map map = new Map();
            MapReader.ReadMap(map);

            //Edit map or calculate logistics center
            UserChoiceManager.ChoiceManager(map);
        }
    }
}
