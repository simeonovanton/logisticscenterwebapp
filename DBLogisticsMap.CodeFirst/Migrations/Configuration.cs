﻿namespace DBLogisticsMap.CodeFirst.Migrations
{
	using System.Data.Entity.Migrations;
	using System.Linq;
	using LogisticsMap.Models;

	public sealed class Configuration : DbMigrationsConfiguration<DBLogisticsMap.CodeFirst.LogisticsMapContext>
	{
		public Configuration()
		{
			AutomaticMigrationsEnabled = true;
			AutomaticMigrationDataLossAllowed = true;
		}

		protected override void Seed(DBLogisticsMap.CodeFirst.LogisticsMapContext context)
		{
			if (!context.Cities.Any())
			{
				var sofia = new City()
				{
					Name = "Sofia"
				};
				var plovdiv = new City()
				{
					Name = "Plovdiv"
				};
				var varna = new City()
				{
					Name = "Varna"
				};
				context.Cities.AddRange(new[] { sofia, plovdiv, varna });
				context.SaveChanges();

				context.Routes.AddRange(new[]
				{
					new Route()
					{
						CityA = sofia,
						CityB = plovdiv,
						Distance = 150
					},
					new Route()
					{
						CityA = plovdiv,
						CityB = varna,
						Distance = 400
					},
					new Route()
					{
						CityA  = sofia,
						CityB = varna,
						Distance = 500
					}
				});
				context.SaveChanges();
			}
		}
	}
}
