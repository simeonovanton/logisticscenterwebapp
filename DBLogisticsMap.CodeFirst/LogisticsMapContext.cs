﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using LogisticsMap.Models;

namespace DBLogisticsMap.CodeFirst
{
	public class LogisticsMapContext : DbContext
	{
		public LogisticsMapContext()
			: base("name=LogisticsMap")
		{ }

		public DbSet<City> Cities { get; set; }

		public DbSet<Route> Routes { get; set; }

		public DbSet<LogisticCenter> LogisticCenters { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			//modelBuilder.Entity<City>()
			//    .HasRequired<Map>(s => s.CurrentMap)
			//    .WithMany(g => g.Cities.Values)
			//    .HasForeignKey<int>(s => s.CurrentMapId);

			modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
		}
	}
}
