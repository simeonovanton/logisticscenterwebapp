﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using DBLogisticsMap.CodeFirst;
using DBLogisticsMap.CodeFirst.Migrations;
using LogisticsMaps.Web.App_Start;

namespace LogisticsMaps.Web
{
	public class MvcApplication : System.Web.HttpApplication
	{
		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
			AutofacConfig.RegisterDependencies();

			Database.SetInitializer(new MigrateDatabaseToLatestVersion<LogisticsMapContext, Configuration>());
		}
	}
}
