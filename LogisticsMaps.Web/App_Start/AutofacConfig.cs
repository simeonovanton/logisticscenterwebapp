﻿using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using DBLogisticsMap.CodeFirst;
using LogisticsMap.BusinessLogic;

namespace LogisticsMaps.Web.App_Start
{
	public class AutofacConfig
	{
		public static void RegisterDependencies()
		{
			var builder = new ContainerBuilder();

			// Register your MVC controllers.
			builder.RegisterControllers(typeof(MvcApplication).Assembly);

			builder.Register(b => new LogisticsMapContext())
				.InstancePerRequest();
            builder.RegisterType<LogisticCenterSolver>().As<ILogisticCenterSolver>().InstancePerRequest();
			// Set the dependency resolver to be Autofac.
			var container = builder.Build();
			DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
		}
	}
}
