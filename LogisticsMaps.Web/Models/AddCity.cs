﻿using System.ComponentModel.DataAnnotations;

namespace LogisticsMaps.Web.Models
{
	public class AddCity
	{
		[Required]
		[StringLength(50, MinimumLength = 3)]
		public string Name { get; set; }
	}
}
