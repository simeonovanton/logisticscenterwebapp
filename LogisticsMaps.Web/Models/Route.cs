﻿namespace LogisticsMaps.Web.Models
{
	public class Route
	{
		public string CityA { get; set; }

		public string CityB { get; set; }

		public int Disctance { get; set; }
	}
}
