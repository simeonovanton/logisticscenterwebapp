﻿using System.Collections.Generic;

namespace LogisticsMaps.Web.Models
{
	public class Index
	{
		public string LogisticCenter { get; set; }

		public IEnumerable<City> Cities { get; set; }

		public IEnumerable<Route> Routes { get; set; }
	}
}
