﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace LogisticsMaps.Web.Models
{
	public class AddRoute
	{
		[Required]
		[Range(1, int.MaxValue)]
		public int CityA { get; set; }

		[Required]
		[Range(1, int.MaxValue)]
		public int Distance { get; set; }

		[Required]
		[Range(1, int.MaxValue)]
		public int CityB { get; set; }

		public IEnumerable<SelectListItem> Cities { get; set; }
	}
}
