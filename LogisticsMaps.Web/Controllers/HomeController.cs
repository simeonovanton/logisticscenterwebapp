﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using DBLogisticsMap.CodeFirst;
using LogisticsMap.BusinessLogic;
using LogisticsMap.Models;
using LogisticsMaps.Web.Models;

namespace LogisticsMaps.Web.Controllers
{
	public class HomeController : Controller
	{
		private readonly LogisticsMapContext dbContext;
		private readonly ILogisticCenterSolver solver;

		public HomeController(LogisticsMapContext dbContext, ILogisticCenterSolver solver)
		{
			this.dbContext = dbContext;
			this.solver = solver;
		}

		public ActionResult Index()
		{
			var cities = dbContext.Cities.ToList();
			var routes = dbContext.Routes.Include(r => r.CityA)
				.Include(r => r.CityB)
				.ToList();
			var center = dbContext.LogisticCenters.Include(nameof(LogisticCenter.City))
				.FirstOrDefault();
			var model = new Index()
			{
				LogisticCenter = center?.City.Name ?? "Not calculated yet",
				Cities = cities.Select(c => new Models.City()
				{
					Id = c.Id,
					Name = c.Name
				}),
				Routes = routes.Select(r => new Models.Route()
				{
					CityA = r.CityA.Name,
					CityB = r.CityB.Name,
					Disctance = r.Distance
				})
			};
			return View(model);
		}

		public ActionResult AddCity()
		{
			return View();
		}

		[HttpPost]
		public ActionResult AddCity(AddCity model)
		{
			if (!ModelState.IsValid)
			{
				return View();
			}

			var city = dbContext.Cities.FirstOrDefault(c => c.Name == model.Name);
			if (city != null)
			{
				ModelState.AddModelError(nameof(model.Name), "Already exists");
				return View();
			}

			city = new LogisticsMap.Models.City()
			{
				Name = model.Name
			};
			dbContext.Cities.Add(city);
			dbContext.SaveChanges();

			return RedirectToAction(nameof(Index));
		}

		public ActionResult AddRoute()
		{
			var model = new AddRoute()
			{
				Cities = GetCitiesAsSelectItems()
			};
			return View(model);
		}

		[HttpPost]
		public ActionResult AddRoute(AddRoute model)
		{
			if (!ModelState.IsValid)
			{
				model.Cities = GetCitiesAsSelectItems();
				return View(model);
			}

			if (model.CityA == model.CityB)
			{
				ModelState.AddModelError(nameof(model.CityB), "Cannot be the same");
				model.Cities = GetCitiesAsSelectItems();
				return View(model);
			}

			var cities = dbContext.Cities.Where(c => c.Id == model.CityA || c.Id == model.CityB)
				.ToList();
			if (!cities.Any(c => c.Id == model.CityA))
			{
				ModelState.AddModelError(nameof(model.CityA), "Not Found");
				model.Cities = GetCitiesAsSelectItems();
				return View(model);
			}

			// TODO: the same for CityB

			var route = dbContext.Routes.FirstOrDefault(r => (r.CityAId == model.CityA || r.CityAId == model.CityB) && (r.CityBId == model.CityA || r.CityBId == model.CityB));
			if (route == null)
			{
				route = new LogisticsMap.Models.Route()
				{
					CityAId = model.CityA,
					CityBId = model.CityB,
					Distance = model.Distance
				};
				dbContext.Routes.Add(route);
				dbContext.SaveChanges();
			}
			else if (route.Distance != model.Distance)
			{
				route.Distance = model.Distance;
				dbContext.SaveChanges();
			}

			return RedirectToAction(nameof(Index));
		}

		[HttpPost]
		public ActionResult Update()
		{

			var lastRouteUpdate = dbContext.Routes.OrderByDescending(r => r.LastUpdate)
				.FirstOrDefault();
			var lastCenterUpdate = dbContext.LogisticCenters.FirstOrDefault();
			if (lastRouteUpdate != null && (lastCenterUpdate == null || lastRouteUpdate.LastUpdate > lastCenterUpdate?.LastUpdate))
			{
				string center = solver.FindLogisticCenter();
				var city = dbContext.Cities.FirstOrDefault(c => c.Name == center);
				if (lastCenterUpdate != null)
				{
					lastCenterUpdate.CityId = city.Id;
					lastCenterUpdate.LastUpdate = DateTime.UtcNow;
				}
				else
				{
					lastCenterUpdate = new LogisticCenter
					{
						CityId = city.Id,
						LastUpdate = DateTime.UtcNow
					};
					dbContext.LogisticCenters.Add(lastCenterUpdate);
				}

				dbContext.SaveChanges();
			}

			return RedirectToAction(nameof(Index));
		}

		[HttpPost]
		public ActionResult DeleteCity(int id)
		{
			if (ModelState.IsValid)
			{
				var city = dbContext.Cities.FirstOrDefault(c => c.Id == id);
				if (city != null)
				{
					var routes = dbContext.Routes.Where(r => r.CityAId == id || r.CityBId == id)
						.ToList();
					foreach (var route in routes)
					{
						dbContext.Routes.Remove(route);
					}

					dbContext.Cities.Remove(city);
					var center = dbContext.LogisticCenters.FirstOrDefault();
					if (center != null)
					{
						if (center.CityId == id)
						{
							dbContext.LogisticCenters.Remove(center);
						}
						else
						{
							center.LastUpdate = DateTime.MinValue;
						}
					}

					dbContext.SaveChanges();
				}
			}

			return RedirectToAction(nameof(Index));
		}

		private IEnumerable<SelectListItem> GetCitiesAsSelectItems()
		{
			var cities = dbContext.Cities.ToList();
			return cities.Select(c => new SelectListItem()
			{
				Text = c.Name,
				Value = c.Id.ToString()
			});
		}
	}
}
