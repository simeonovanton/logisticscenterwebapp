﻿using System;

namespace LogisticsMap.Models
{
	public class LogisticCenter
	{
		public int Id { get; set; }

		public int CityId { get; set; }

		public virtual City City { get; set; }

		public DateTime LastUpdate { get; set; } = DateTime.UtcNow;
	}
}
