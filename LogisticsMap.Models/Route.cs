﻿using System;
using System.ComponentModel.DataAnnotations;

namespace LogisticsMap.Models
{
	public class Route
	{
		public int Id { get; set; }

		[Required]
		public int CityAId { get; set; }

		public virtual City CityA { get; set; }

		[Required]
		public int CityBId { get; set; }

		public virtual City CityB { get; set; }

		[Required]
		[Range(1, int.MaxValue)]
		public int Distance { get; set; }

		[Required]
		public DateTime LastUpdate { get; set; } = DateTime.UtcNow;
	}
}
