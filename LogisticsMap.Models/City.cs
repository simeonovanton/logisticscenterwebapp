﻿using System.ComponentModel.DataAnnotations;

namespace LogisticsMap.Models
{
	public class City
	{
		public int Id { get; set; }

		[Required]
		[StringLength(50, MinimumLength = 3)]
		public string Name { get; set; }
	}
}
