﻿using DBLogisticsMap.CodeFirst;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace LogisticsMap.BusinessLogic
{
    public class LogisticCenterSolver : ILogisticCenterSolver
    {
        private readonly LogisticsMapContext context;

        public LogisticCenterSolver(LogisticsMapContext context)
        {
            this.context = context;
        }

        private Map MapLoader()
        {
            var map = new Map();
            var routes = context.Routes.ToList();

            foreach (var item in routes)
            {
                var cityA = item.CityA.Name;
                var cityB = item.CityB.Name;
                var distance = item.Distance;
                map.AddCity(cityA);
                map.AddCity(cityB);
                map.AddRoad(cityA, cityB, distance);
            }

            return map;
        }

        public string FindLogisticCenter()
        {
            var map = MapLoader();
            return UserChoiceManager.CalculateLogisticCenter(map).Name;
        }
    }
}
