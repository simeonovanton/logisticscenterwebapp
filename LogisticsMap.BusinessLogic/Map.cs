﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LogisticsMap.BusinessLogic
{
    public class Map
    {
        public Dictionary<string, City> Cities { get; set; }
        public City LogisticCenter { get; set; }

        public Map()
        {
            Cities = new Dictionary<string, City>();
            LogisticCenter = null;
        }

        public void AddCity(string name)
        {
            if (!Cities.ContainsKey(name))
            {
                Cities.Add(name, new City(name));
            }
            else
            {
                Console.WriteLine("Such city already exists!");
            }
            
        }

        public void ChangeCityName(string oldName, string newName)
        {
            City city = Cities[oldName];
            city.Name = newName;
            try
            {
                Cities.Remove(oldName);
            }
            catch (KeyNotFoundException e)
            {

                throw new KeyNotFoundException("Could NOT delete a city while ranaming." + " " + e.Message);
            }
            Cities.Add(newName, city);
        }

        public void DeleteCity(string name)
        {
            //TODO remove city from ChildCities first!
            foreach (var city in this.Cities[name].ChildCities.Keys)
            {
                city.ChildCities.Remove(GetCity(name));
            }

            //Remove city record from Map.Cities
            try
            {
                Cities.Remove(name);
            }
            catch (KeyNotFoundException e)
            {

                throw new KeyNotFoundException("Could NOT delete a city." + " " + e.Message);
            }
        }

        public City GetCity(string cityName)
        {
            try
            {
                return Cities[cityName];
            }
            catch (KeyNotFoundException e)
            {

                throw new KeyNotFoundException($"Couldn't find such a city name: {cityName} " + e.Message);
            }
            
        }

        public void AddRoad(string cityA, string cityB)
        {
            City cityACity;
            City cityBCity;
            try
            {
                cityACity = this.GetCity(cityA);

            }
            catch (KeyNotFoundException e)
            {

                throw new KeyNotFoundException($"Couldn't find such a city name: {cityA} " + e.Message);
            }

            try
            {
                cityBCity = this.GetCity(cityB);
            }
            catch (KeyNotFoundException e)
            {

                throw new KeyNotFoundException($"Couldn't find such a city name: {cityB} " + e.Message);
            }

            cityACity.ChildCities.Add(cityBCity, 1);
            cityBCity.ChildCities.Add(cityACity, 1);
        }

        public void AddRoad(string cityA, string cityB, int distance)
        {
            City cityACity;
            City cityBCity;
            try
            {
                cityACity = this.GetCity(cityA);

            }
            catch (KeyNotFoundException e)
            {

                throw new KeyNotFoundException($"Couldn't find such a city name: {cityA} " + e.Message);
            }

            try
            {
                cityBCity = this.GetCity(cityB);
            }
            catch (KeyNotFoundException e)
            {

                throw new KeyNotFoundException($"Couldn't find such a city name: {cityB} " + e.Message);
            }
            cityACity.ChildCities.Add(cityBCity, distance);
            cityBCity.ChildCities.Add(cityACity, distance);
        }

        public void DeleteRoad(string cityA, string cityB)
        {
            City cityACity;
            City cityBCity;
            try
            {
                cityACity = this.GetCity(cityA);

            }
            catch (KeyNotFoundException e)
            {

                throw new KeyNotFoundException($"Couldn't find such a city name: {cityA} " + e.Message);
            }

            try
            {
                cityBCity = this.GetCity(cityB);
            }
            catch (KeyNotFoundException e)
            {

                throw new KeyNotFoundException($"Couldn't find such a city name: {cityB} " + e.Message);
            }

            try
            {
                cityACity.ChildCities.Remove(cityBCity);
            }
            catch (KeyNotFoundException e)
            {

                throw new KeyNotFoundException($"Couldn't find such city: {cityBCity.Name} " + e.Message);
            }

            try
            {
                cityBCity.ChildCities.Remove(cityACity);
            }
            catch (KeyNotFoundException e)
            {

                throw new KeyNotFoundException($"Couldn't find such city: {cityACity.Name} " + e.Message);
            }

        }

        public bool ContainsCity(string cityName)
        {
            bool result = false;
            if (Cities.ContainsKey(cityName))
            {
                result = true;
            }
            return result;
        }



    }
}
