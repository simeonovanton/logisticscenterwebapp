﻿using System;
using System.Collections.Generic;
using Wintellect.PowerCollections;

namespace LogisticsMap.BusinessLogic
{
    static class UserChoiceManager
    {
        public static void ChoiceManager(Map map)
        {
            while (true)
            {
                Console.Write("Please, enter your action:\n" +
                "1. Add city\n" +
                "2. Change city name\n" +
                "3. Delete city - in format (cityName)\n" +
                "4. Add road\n" +
                "5. Delete road - in format(cityA cityB)\n" +
                "6. CALCULATE LOGISTIC CENTER\n" +
                "7. Exit application\n" +
                "   Choice: ");

                int choice;
                if (!int.TryParse(Console.ReadLine(), out choice))
                {
                    continue;
                }

                switch (choice)
                {
                    case 1:
                        AddCity(map);
                        break;
                    case 2:
                        ChangeCityName(map);
                        break;
                    case 3:
                        DeleteCity(map);
                        break;
                    case 4:
                        AddRoad(map);
                        break;
                    case 5:
                        DeleteRoad(map);
                        break;
                    case 6:
                        CalculateLogisticCenter(map);
                        break;
                    case 7: return;

                    default:
                        Console.WriteLine("Unavailable choice. Please try again!");
                        break;
                }
            }
        }

        public static void AddCity(Map map)
        {
            Console.Write("Enter city name: ");
            string cityName = Console.ReadLine();
            map.AddCity(cityName);
        }

        public static void ChangeCityName(Map map)
        {
            Console.Write("Enter oldName and newName in format /oldName newName/: ");
            string input = Console.ReadLine();
            string[] members = input.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            string oldName = members[0];
            string newName = members[1];
            map.ChangeCityName(oldName, newName);
        }

        public static void DeleteCity(Map map)
        {
            Console.Write("Enter name of the city to delete: ");
            string cityName = Console.ReadLine();
            map.DeleteCity(cityName);
        }

        public static void AddRoad(Map map)
        {
            Console.Write("Enter road between two cities. Possible format:\n" +
                "(cityA cityB) - distance is default 1,\n" +
                "or (cityA cityB distance(integer value)): ");
            string input = Console.ReadLine();
            string[] members = input.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            switch (members.Length)
            {
                case 2:
                    string cityA = members[0];
                    string cityB = members[1];
                    map.AddRoad(cityA, cityB);
                    break;
                case 3:
                    cityA = members[0];
                    cityB = members[1];
                    int distance = int.Parse(members[2]);
                    map.AddRoad(cityA, cityB, distance);
                    break;
                default:
                    Console.WriteLine("Unconsistent input.");
                    break;
            }
        }

        public static void DeleteRoad(Map map)
        {
            Console.Write("Enter the names of cities to delete road\n" +
                "in format(cityNameA cityNameB): ");
            string input = Console.ReadLine();
            string[] members = input.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            string cityA = members[0];
            string cityB = members[1];
            map.DeleteRoad(cityA, cityB);
        }

        public static void ClearAllCitiesProperties(Map map)
        {
            foreach (var city in map.Cities.Values)
            {
                city.LastCity = null;
                city.BestDistance = int.MaxValue;
                city.MaxDistanceCity = null;
                city.MaxDistance = 0;
                city.Visited = false;
                city.IsLogisticCenter = false;
            }
        }

        public static City CalculateLogisticCenter(Map map)
        {
            Dictionary<int, Queue<City>> citiesByMaxDistance = new Dictionary<int, Queue<City>>();
            List<int> keys = new List<int>();
            foreach (var record in map.Cities)
            {
                ClearAllCitiesProperties(map);
                string cityName = record.Key;
                City city = record.Value;
                MapTraverser.RunDijkstraFromStartCity(map, cityName);

                int cityMaxDistance = city.MaxDistance;
                if (cityMaxDistance == 0)
                {
                    continue;   //Exclude cities that are not connected to map by road
                }
                keys.Add(cityMaxDistance);
                if (!citiesByMaxDistance.ContainsKey(cityMaxDistance))
                {
                    citiesByMaxDistance.Add(cityMaxDistance, new Queue<City>());
                    citiesByMaxDistance[cityMaxDistance].Enqueue(city);
                }
                else
                {
                    citiesByMaxDistance[cityMaxDistance].Enqueue(city);
                }
            }
            keys.Sort();
            int minKey = keys[0];
            City logisticCenter = citiesByMaxDistance[minKey].Dequeue();
            logisticCenter.IsLogisticCenter = true;
            map.LogisticCenter = logisticCenter;
            return logisticCenter;
            //Console.WriteLine($"LOGISTIC CENTER should be: {logisticCenter.Name}\n"); //Print LOGISTIC CENTER name
        }
    }
}
