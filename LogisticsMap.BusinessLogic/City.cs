﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LogisticsMap.BusinessLogic
{
    public class City : IComparable<City>
    {
        public string Name { get; set; }
        public Dictionary<City, int> ChildCities { get; set; } // holds a set of records to the neighbor cities and distances to them
        public City LastCity { get; set; } // holds the name of the previous city after map traversing for shortest path from a given start city. Used for eventually extention of the functionality for back-track
        public int BestDistance { get; set; } // holds the minimum temporary distance from another startCity during MapTraverse 
        public City MaxDistanceCity { get; set; }
        public int MaxDistance { get; set; } // the max distance to other cities after Dijkstra
        public bool Visited { get; set; }
        public bool IsLogisticCenter { get; set; }

        public City(string name)
        {
            Name = name;
            ChildCities = new Dictionary<City, int>();
            LastCity = null;
            BestDistance = int.MaxValue;
            MaxDistanceCity = null;
            MaxDistance = 0;
            Visited = false;
            IsLogisticCenter = false;
        }

        public int CompareTo(City other) //Compared by BestDistance property - for OrderedBag data structure internal sort
        {
            int compareToBestDistance = this.BestDistance.CompareTo(other.BestDistance);
            return compareToBestDistance == 0 ? this.Name.CompareTo(other.Name) : compareToBestDistance;
        }
    }
}
