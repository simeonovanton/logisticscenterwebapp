﻿using DBLogisticsMap.CodeFirst;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogisticsMap.BusinessLogic
{
    public interface ILogisticCenterSolver
    {
         string FindLogisticCenter();
    }
}
