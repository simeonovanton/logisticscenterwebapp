﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;


namespace LogisticsMap.BusinessLogic
{
    static class MapReader
    {
        public const string fileName = "GraphCities.txt";
        public static void ReadMap(Map map)
        {
            try
            {
                StreamReader reader = new StreamReader(fileName);
                using (reader)
                {
                    while (!reader.EndOfStream)
                    {
                        string input = reader.ReadLine();
                        string[] members = input.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        string cityA = members[0];
                        string cityB = members[1];
                        int distance = int.Parse(members[2]);

                        if (!map.ContainsCity(cityA))
                        {
                            map.AddCity(cityA);
                        }

                        if (!map.ContainsCity(cityB))
                        {
                            map.AddCity(cityB);
                        }

                        // Add route between cityA and cityB - including distance
                        map.AddRoad(cityA, cityB, distance);
                    }
                }
            }
            catch (FileNotFoundException e)
            {
                throw new FileNotFoundException($"File couldn't be found: {fileName} !!! " + e.Message);
            }
        }

    }
}
