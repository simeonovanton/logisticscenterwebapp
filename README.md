Да се направи уеб приложение с база данни, в което могат да се добавят и редактират:

    Населени места
    Пътища (Начален град, Краен град)
    Логистичен център

 През интерфейса на приложението трябва да могат да се добавят и редактират населените места и пътища.

 

При натискане на бутон да се изпълни следния алгоритъм:

 

Даден е район с N населени места (броя на въведените градове в базата) и M пътища (броя на пътищата в базата), всеки откоито свързва две различни населени места на района.

 Да се избира такова населено място C, че най-отдалеченото от останалите населени места да е колкото може по-близо до C.След като се избере такова място C да се запише в базата в таблицата като логистичен център. Трябва да има валидация, акопри натискане на бутона няма промяна в населените места и пътищата (т.е. били са същите като последното изпълнение), да небъде създаван нов логистичен център.
